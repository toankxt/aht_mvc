<?php

    namespace aht_mvc\Models;

    use aht_mvc\Models\TaskResourceModel;

    class TaskRepository
    {
        private $taskResourceModel;

        public function __construct()
        {
            return $this->taskResourceModel = new TaskResourceModel();
        }

        public function add($model)
        {   
            return $this->taskResourceModel->save($model);
        }

        public function edit($model)
        {
            return $this->taskResourceModel->save($model);
        }

        public function delete($id)
        {
            return $this->taskResourceModel->delete($id);
        }

        public function get($id) 
        {
            return $this->taskResourceModel->getTaskById($id);
        }

        public function getAllTasks()
        {
            return $this->taskResourceModel->getAllTasks();
        }
    }
