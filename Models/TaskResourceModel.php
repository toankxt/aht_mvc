<?php

    namespace aht_mvc\Models;

    use aht_mvc\Core\ResourceModel;
    use aht_mvc\Models\Task;

    class TaskResourceModel extends ResourceModel
    {
        public function __construct()
        {
            return $this->_init("tasks", "id", new Task);
        }
    }
