<?php

    namespace aht_mvc\Models;

    use aht_mvc\Core\Model;
    use aht_mvc\Config\Database;

    class Task extends Model 
    {
        protected $id;
        protected $title;
        protected $description;
        protected $created_at;
        protected $updated_at;

        public function getId()
        {
            return $this->id;
        }

        public function getTitle()
        {
            return $this->title;
        }

        public function getDescription()
        {
            return $this->description;
        }

        public function getCreatedAt()
        {
            return $this->created_at;
        }

        public function getUpdatedAt()
        {
            return $this->updated_at;
        }

        public function setId($id)
        {
            $this->id = $id;
        }

        public function setTitle($title)
        {
            $this->title = $title;
        }

        public function setDescription($description)
        {
            $this->description = $description;
        }

        public function setCreatedAt($created_at)
        {
            $this->created_at = $created_at;
        }

        public function setUpdateAt($updated_at)
        {
            $this->updated_at = $updated_at;
        }
    }
