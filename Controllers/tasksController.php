<?php

    namespace aht_mvc\Controllers;

    use aht_mvc\Core\Controller;
    use aht_mvc\Models\Task;
    use aht_mvc\Models\TaskRepository;

    class tasksController extends Controller
    {
        function index()
        {
            $tasks      = new Task();

            $taskRepo   = new TaskRepository();

            $d['tasks'] = $taskRepo->getAllTasks();
            $this->set($d);
            $this->render("index");
        }

        function create()
        {
            if (isset($_POST['title']))
            {
                $title      = $_POST['title'];
                $desc       = $_POST['description'];
                $createdAt  = date('Y-m-d H:i:s');

                if (empty($title)) 
                {
                    $this->render("create");
                    die();
                } else {
                    $task = new Task();

                    $taskRepo = new TaskRepository();

                    $task->setTitle($title);
                    $task->setDescription($desc);
                    $task->setCreatedAt($createdAt);
                    
                    $check = $taskRepo->add($task);
        
                    if ($check)
                    {
                        header("Location: " . WEBROOT . "tasks/index");
                    }
                }
            }

            $this->render("create");
        }

        function edit($id)
        {
            $task       = new Task();

            $taskRepo   = new TaskRepository();

            $d["task"]  = $taskRepo->get($id);

            if (isset($_POST['title']))
            {
                $title      = $_POST['title'];
                $desc       = $_POST['description'];
                $updatedAt  = date('Y-m-d H:i:s');

                if (!empty($title))
                {
                    $task->setId($id);
                    $task->setTitle($title);
                    $task->setDescription($desc);
                    $task->setUpdateAt($updatedAt);

                    $check = $taskRepo->edit($task);

                    if ($check)
                    {
                        header("Location: " . WEBROOT . "tasks/index");
                    }

                }
            }
            $this->set($d);
            $this->render("edit");
        }

        function delete($id)
        {
            $task       = new Task();

            $taskRepo   = new TaskRepository();

            $check      = $taskRepo->delete($id);

            if ($check)
            {
                header("Location: " . WEBROOT . "tasks/index");
            }
        }
    }
