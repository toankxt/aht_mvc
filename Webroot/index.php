<?php

    require __DIR__ . '../../vendor/autoload.php';

    define('WEBROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_NAME"]));
    define('ROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_FILENAME"]));

    // use aht_mvc\Config\Core;
    // use aht_mvc\router;
    // use aht_mvc\request;
    use aht_mvc\dispatcher;

    $dispatch = new Dispatcher();
    $dispatch->dispatch();
