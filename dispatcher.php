<?php

    namespace aht_mvc;

    use aht_mvc\request;
    use aht_mvc\router;

    class Dispatcher
    {

        private $request;

        public function dispatch()
        {
            $this->request = new Request();
            
            Router::parse($this->request->url, $this->request);
            
            $controller = $this->loadController();

            call_user_func_array([$controller, $this->request->action], $this->request->params);
        }

        public function loadController()
        {
            $name = $this->request->controller . "Controller";
            $controller = '\\aht_mvc\\Controllers\\' . $name;
            return new $controller();
        }
    }
    