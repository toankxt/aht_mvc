<?php

    namespace aht_mvc\Core;

    use aht_mvc\Core\ResourceModelInterface;
    use PDO;
    use aht_mvc\Config\Database;
    use aht_mvc\Core\Model;

    class ResourceModel implements ResourceModelInterface
    {
        private $table;
        private $id;
        private $model;
        private $sql;

        public function demoUpdate()
        {
            return $this;
        }

        public function _init($table, $id, $model)
        {
            $this->table = $table;
            $this->id = $id;
            $this->model = $model;
        }

        public function save($model)
        {
            if($model->getId() == null)
            {   
                // create
                $str_key = '';
                $str_value = '';
                $modelCreate = array();

                $title = $model->getTitle();
                $desc = $model->getDescription();

                $properties = $this->model->getProperties($model);
                unset($properties['id']);

                foreach ($properties as $key => $value) 
                {
                    $str_key .= $key . ', ';
                    $str_value .= ':' . $key . ', ';
                    $modelCreate[$key] = $properties[$key];
                }

                $str_key = trim($str_key, ", ");
                $str_value = trim($str_value, ", ");
                $sql = "INSERT INTO $this->table ($str_key) VALUES ($str_value)";

                unset($modelCreate['id']);
            
                $req = Database::getBdd()->prepare($sql);
        
                return $req->execute($modelCreate);
            }
            else 
            {
                // update
                $str_update = '';
                $modelUpdate = array();

                $id = $model->getId();
                $title = $model->getTitle();
                $desc = $model->getDescription();
                $updateAt = $model->getUpdatedAt();

                $properties = $this->model->getProperties($model);

                unset($properties['id']);
                unset($properties['created_at']);

                foreach ($properties as $key => $value) 
                {
                    $str_update .= $key . '=' . ':' . $key . ', ';
                    $modelUpdate[$key] = $properties[$key];
                }

                $str_update = trim($str_update, ", ");
                
                $sql = "UPDATE $this->table SET " . $str_update . " WHERE id=:id";

                $modelUpdate['id'] = $id;

                $req = Database::getBdd()->prepare($sql);

                return $req->execute($modelUpdate);
            }
        }

        public function delete($id)
        {
            $modelDelete[$this->id] = $id;

            $sql = "DELETE FROM $this->table WHERE $this->id=" . $id;

            $req = Database::getBdd()->prepare($sql);
            
            return $req->execute($modelDelete);
        }

        public function getAllTasks()
        {   
            $results = array();
            $sql = "SELECT * FROM " . $this->table;

            $req = Database::getBdd()->prepare($sql);
            $req->execute();
            $res = $req->fetchAll();
            
            foreach ($res as $value) {
                array_push($results, $value);
            }

            return $results;
        }

        public function getTaskById($id)
        {
            $sql = "SELECT * FROM $this->table WHERE $this->id = " . $id;

            $req = Database::getBdd()->prepare($sql);

            $req->execute();

            return $req->fetch();

        }
    }
