<?php

    namespace aht_mvc\Core;

    use aht_mvc\Helpper;

    class Model
    {
        public function getProperties($model)
        {
            return get_object_vars($model);
        }
    }
